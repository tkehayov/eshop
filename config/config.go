package config

import "gopkg.in/mgo.v2"

// const (
// 	//Database for production name
// 	Database string = "eshop"
// 	//DatabaseTest for testing name
// 	DatabaseTest string = "eshop_test"
// 	// URL to connect
// 	URL string = "127.0.0.1"
// )

// DatabaseConfigurator basic database configuration
type DatabaseConfigurator interface {
	GetDb() string
	GetURL() string
	GetSession() *mgo.Session
}

//
// DatabaseConfig implementation
type DatabaseConfig struct{}

// GetDb return database name
func (db DatabaseConfig) GetDb() string {
	return "eshop"
}

//GetURL return database URL
func (db DatabaseConfig) GetURL() string {
	return "127.0.0.1"
}

//GetSession from database
func (db DatabaseConfig) GetSession() *mgo.Session {
	session, err := mgo.Dial(db.GetURL())
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	return session
}
