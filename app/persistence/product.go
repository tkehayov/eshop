package persistence

import (
	"log"

	"github.com/emat/config"
	"gopkg.in/mgo.v2/bson"
)

const (
	productCollection string = "product"
)

// Productor is abstract for repository
type Productor interface {
	Add(productEntity ProductEntity)
	Get(name string) []ProductListEntity
	GetByIds(ids []bson.ObjectId) []ProductEntity
	GetID(id string) ProductEntity
}

// ProductListEntity using for retrieving products
type ProductListEntity struct {
	ID       bson.ObjectId `json:"id" bson:"_id"`
	Name     string        `json:"name"`
	Price    float64       `json:"price"`
	ImageURL string        `json:"image_url"`
}

// ProductEntity entity
type ProductEntity struct {
	Name      string   `json:"name"`
	Price     float64  `json:"price"`
	ImageURL  string   `json:"imageUrl"`
	ImageMini []string `json:"imageMini"`
	Featured  bool     `json:"featured"`
}

type product struct {
	config config.DatabaseConfigurator
}

//NewProduct builder for generating product
func NewProduct(config config.DatabaseConfigurator) Productor {
	return &product{config}
}

//Add new products
func (p product) Add(product ProductEntity) {
	defer p.config.GetSession().Close()

	c := p.config.GetSession().DB(p.config.GetDb()).C(productCollection)

	pe := ProductEntity(product)
	err := c.Insert(pe)
	if err != nil {
		log.Fatal(err)
	}
}

//Get products
func (p product) Get(name string) []ProductListEntity {
	defer p.config.GetSession().Close()
	c := p.config.GetSession().DB(p.config.GetDb()).C(productCollection)
	pe := []ProductListEntity{}

	err := c.Find(bson.M{"featured": true}).Limit(10).All(&pe)

	if err != nil {
		log.Fatal(err)
	}
	return pe
}

func (p product) GetByIds(ids []bson.ObjectId) []ProductEntity {
	defer p.config.GetSession().Close()
	pe := []ProductEntity{}

	c := p.config.GetSession().DB(p.config.GetDb()).C(productCollection)
	c.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&pe)

	return pe
}

//GetId get product
func (p product) GetID(id string) ProductEntity {
	defer p.config.GetSession().Close()
	pe := ProductEntity{}

	c := p.config.GetSession().DB(p.config.GetDb()).C(productCollection)
	c.Find(bson.M{"_id": bson.ObjectIdHex(id)}).One(&pe)

	return pe
}
