package persistence

import (
	"log"
	"testing"

	"gopkg.in/mgo.v2"
)

func TestAddAndGetOne(t *testing.T) {
	f := FakeDatabaseConfigurator{}
	NewProduct(f)
}

//GetSession dd
type FakeProductor struct {

	//Add faking new obje
}

//Add new products
func (p FakeProductor) Add(name string) {
	f := FakeDatabaseConfigurator{}

	defer f.GetSession().Close()
	c := f.GetSession().DB(p.config.GetDb()).C(collection)
	pe := ProductEntity{name}
	err := c.Insert(pe)
	if err != nil {
		log.Fatal(err)
	}
}

type FakeDatabaseConfigurator struct{}

func (FakeDatabaseConfigurator) GetDb() string {
	return "eshop"
}
func (FakeDatabaseConfigurator) GetURL() string {
	return "127.0.0.1"
}

func (FakeDatabaseConfigurator) GetSession() *mgo.Session {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	session.SetMode(mgo.Monotonic, true)

	return session
}

// func (db DatabaseConfig) GetSession() *mgo.Session {
// 	session, err := mgo.Dial(config.URL)
// 	if err != nil {
// 		panic(err)
// 	}
// 	session.SetMode(mgo.Monotonic, true)
//
// 	return session
// }
