package persistence

import (
	"log"

	"github.com/emat/config"
)

const (
	checkoutCollection string = "checkout"
)

//Checkoutor is abstract for repository
type Checkoutor interface {
	Add(ce CheckoutEntity)
}

//CheckoutEntity persist entity
type CheckoutEntity struct {
	AdditionalInfo string     `json:"additionalInfo" bson:",omitempty"`
	District       string     `json:"district" bson:",omitempty"`
	Floor          string     `json:"floor" bson:",omitempty"`
	Location       string     `json:"location" validate:"nonzero"`
	Street         string     `json:"street" bson:",omitempty"`
	StreetNumber   string     `json:"streetNumber" bson:",omitempty"`
	Individual     Individual `json:"individual"`
	Company        Company    `json:"company"`
}

//Individual checkout individual
type Individual struct {
	Email string `json:"email"`
	Name  string `json:"name"`
	Phone string `json:"phone"`
}

// Company checkout company
type Company struct {
	AcountablePerson string `json:"acountablePerson"`
	Address          string `json:"address"`
	Email            string `json:"email"`
	Name             string `json:"name"`
	Phone            string `json:"phone"`
}
type checkout struct {
	config config.DatabaseConfigurator
}

// NewCheckout builder
func NewCheckout(config config.DatabaseConfigurator) Checkoutor {
	return &checkout{config}
}

func (ch checkout) Add(ce CheckoutEntity) {
	defer ch.config.GetSession().Close()

	c := ch.config.GetSession().DB(ch.config.GetDb()).C(checkoutCollection)

	che := CheckoutEntity(ce)
	err := c.Insert(che)
	if err != nil {
		log.Fatal(err)
	}
}
