package route

import (
	"net/http"

	"github.com/emat/app/web"
	"github.com/gorilla/mux"
)

//Route all routing here
func Route() http.Handler {
	r := mux.NewRouter()
	r.HandleFunc("/product", web.AddProduct).Methods("POST")
	r.HandleFunc("/product", web.ListProduct).Methods("GET")
	r.HandleFunc("/product/{id}", web.GetProduct).Methods("GET")

	r.HandleFunc("/products", web.RequestProducts).Methods("GET")

	r.HandleFunc("/cart", web.AddCart).Methods("POST")
	r.HandleFunc("/cart", web.GetCart).Methods("GET")

	r.HandleFunc("/checkout", web.CheckoutProduct)
	r.HandleFunc("/checkout", web.CheckoutProduct).Methods("POST")

	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))

	return r
}
