package web

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/emat/app/persistence"
	"github.com/emat/config"
	"github.com/gorilla/mux"
)

//AddProduct new product in repository
func AddProduct(w http.ResponseWriter, r *http.Request) {
	db := config.DatabaseConfig{}
	dbor := config.DatabaseConfigurator(db)
	prod := persistence.NewProduct(dbor)

	// dummy productss
	images := []string{"1-mini.jpg", "2-mini.jpg"}
	dummyProd := persistence.ProductEntity{Name: "Digital Electro Goods", Price: 2.43, ImageURL: "1.jpg", ImageMini: images, Featured: true}

	prod.Add(dummyProd)
}

// ListProduct products
func ListProduct(w http.ResponseWriter, r *http.Request) {
	db := config.DatabaseConfig{}
	dbor := config.DatabaseConfigurator(db)
	prod := persistence.NewProduct(dbor)
	pe := prod.Get("Digital Electro Goods")

	res, err := json.Marshal(pe)

	s := string(res)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprint(w, s)
}

// GetProduct by id
func GetProduct(w http.ResponseWriter, r *http.Request) {
	// ra := mux.NewRouter()
	vars := mux.Vars(r)

	db := config.DatabaseConfig{}
	dbor := config.DatabaseConfigurator(db)
	prod := persistence.NewProduct(dbor)
	pe := prod.GetID(vars["id"])

	res, err := json.Marshal(pe)

	s := string(res)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Fprint(w, s)
}

type Query struct {
	Products []Product `xml:"product"`
}

// Product lists
type Product struct {
	ID              int             `xml:"id"`
	CatalogNUM      string          `xml:"catalog_num"`
	Kkprice         float64         `xml:"kkprice"`
	Model           string          `xml:"model"`
	Name            string          `xml:"name"`
	PictureName     string          `xml:"picture_name"`
	Price           float64         `xml:"price"`
	PriceWAT        float64         `xml:"price_dds"`
	RealPriceWAT    float64         `xml:"realPrice_BGL"`
	RealPriceBGLWAT float64         `xml:"realPrice_BGL_dds"`
	KkpriceBGL      float64         `xml:"kkprice_BGL"`
	KkpriceBGLWAT   float64         `xml:"kkprice_BGL_dds"`
	RealPrice       float64         `xml:"realPrice"`
	Code            string          `xml:"code"`
	Description     string          `xml:"description"`
	Characteristics Characteristics `xml:"characteristics"`
}

// Characteristics
type Characteristics struct {
	Characteristic []Characteristic `xml:"characteristic"`
}

// Characteristic list
type Characteristic struct {
	Description string `xml:"description"`
	Name        string `xml:"name"`
}

// RequestProducts retrieve from server
func RequestProducts(w http.ResponseWriter, r *http.Request) {
	xmlFile, err := http.Get("http://www.polycomp.bg/service/data/v1/products/test/0006301572674/0006301572485/0006302002587")
	// xmlFile, err := os.Open("Products.xml")
	if err != nil {
		fmt.Println("Error opening file:", err)
		return
	}
	defer xmlFile.Body.Close()

	b, _ := ioutil.ReadAll(xmlFile.Body)
	var q Query
	xml.Unmarshal(b, &q)

	fmt.Println(q.Products)
}
