package web

import (
	"encoding/json"
	"fmt"
	"net/http"

	validator "gopkg.in/validator.v2"

	"github.com/emat/app/persistence"
	"github.com/emat/config"
)

// CheckoutProduct proceed checkout
func CheckoutProduct(w http.ResponseWriter, r *http.Request) {
	db := config.DatabaseConfig{}
	dbor := config.DatabaseConfigurator(db)
	checkout := persistence.NewCheckout(dbor)
	var co persistence.CheckoutEntity
	err := json.NewDecoder(r.Body).Decode(&co)

	if err != nil {
		fmt.Print(err.Error())
	}

	errs := validator.Validate(co)

	if errs != nil {
		fmt.Print(errs.Error())
		return
	}

	checkout.Add(co)
}
