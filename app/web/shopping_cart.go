package web

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"github.com/emat/app/persistence"
	"github.com/emat/config"
)

//ShopCart shopcart dummy
type ShopCart struct {
	Product string `json:"product"`
}

// AddCart add product to shopping cart
func AddCart(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err.Error())
	}

	var shopCart = new(ShopCart)
	unmErr := json.Unmarshal(body, &shopCart)
	if unmErr != nil {
		fmt.Println(unmErr.Error())
	}

	shopCartCookies, _ := r.Cookie("shop_cart")
	var shopCartCookiesList []string

	if shopCartCookies != nil {
		shopCartCookiesList = strings.Split(shopCartCookies.Value, ",")
	}
	shopCartCookiesList = append(shopCartCookiesList, shopCart.Product)
	// fmt.Print(shopCartCookiesList)
	cookie := addCookie(*shopCart, shopCartCookiesList)
	http.SetCookie(w, cookie)
}

// GetCart list all products from shopping cart
func GetCart(w http.ResponseWriter, r *http.Request) {
	db := config.DatabaseConfig{}
	dbor := config.DatabaseConfigurator(db)
	prod := persistence.NewProduct(dbor)

	cookie, _ := r.Cookie("shop_cart")

	ids := strings.Split(cookie.Value, ",")

	objIds := make([]bson.ObjectId, len(ids))
	for i := range ids {
		objIds[i] = bson.ObjectIdHex(ids[i])
	}
	// err := json.Unmarshal([]byte(s),ss &pro)

	pa := prod.GetByIds(objIds)
	b, err := json.Marshal(pa)
	if err != nil {
		fmt.Print(err.Error())
	}

	fmt.Fprintln(w, string(b))
}

func addCookie(shopCart ShopCart, s []string) *http.Cookie {
	expiration := time.Now().Add(365 * 24 * time.Hour)

	return &http.Cookie{Name: "shop_cart", Value: strings.Join(s, ","), Expires: expiration}
}
