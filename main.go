package main

import (
	"net/http"

	"github.com/emat/app/route"
)

func main() {
	r := route.Route()

	http.Handle("/", r)
	http.ListenAndServe(":3001", nil)

}
